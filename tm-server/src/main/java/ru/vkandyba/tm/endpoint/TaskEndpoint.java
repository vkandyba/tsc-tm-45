package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.ITaskEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.TaskDTO;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllTasks(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        return serviceLocator.getTaskDtoService().findAll(sessionDTO.getUserId());
    }

    @Override
    public void addTask(@NotNull SessionDTO sessionDTO, @NotNull TaskDTO task) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().add(sessionDTO.getUserId(),task);
    }

    @Override
    public void finishTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().finishById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void finishTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().finishByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void removeTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().removeById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void removeTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    public TaskDTO showTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        return serviceLocator.getTaskDtoService().findById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void startTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().startById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void startTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().startByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void updateTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().updateById(sessionDTO.getUserId(), taskId, name, description);
    }

    @Override
    public void removeTaskWithTasksById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().removeById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void clearTasks(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().clear(sessionDTO.getUserId());
    }

    @Override
    public void unbindTaskFromProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().unbindTaskToProjectById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public @Nullable List<TaskDTO> showAllTasksByProjectId(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        return serviceLocator.getTaskDtoService().findAllTaskByProjectId(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void bindTaskToProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getTaskDtoService().bindTaskToProjectById(sessionDTO.getUserId(), projectId, taskId);
    }

}
