package ru.vkandyba.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.persistence.EntityManager;

public class SessionDtoRepository extends AbstractDtoRepository<SessionDTO> {

    public SessionDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }


}
