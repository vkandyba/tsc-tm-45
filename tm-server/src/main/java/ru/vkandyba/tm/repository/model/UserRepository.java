package ru.vkandyba.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public User findById(@NotNull String id){
        List<User> list = entityManager
                .createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public User findByLogin(@NotNull String login){
        List<User> list = entityManager
                .createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public void removeByLogin(@NotNull String login){
        User user = findByLogin(login);
        entityManager.remove(user);
    }

    public void updateUser(@NotNull String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName){
        User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        entityManager.merge(user);
    }

    public void lockUserByLogin(@NotNull String login){
        User user = findByLogin(login);
        user.setLocked(true);
        entityManager.merge(user);
    }

    public void unlockUserByLogin(@NotNull String login){
        User user = findByLogin(login);
        user.setLocked(false);
        entityManager.merge(user);
    }

}
