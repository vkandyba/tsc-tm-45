package ru.vkandyba.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "app_sessions")
public class Session extends AbstractBusinessEntity implements Cloneable{

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

    @Column
    @Nullable
    private Long timestamp;

    @Column
    @Nullable
    private String signature;

}
